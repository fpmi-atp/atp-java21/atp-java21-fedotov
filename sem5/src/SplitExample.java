import java.util.regex.Pattern;

public class SplitExample {
    /*

     */
    public static void main(String[] args) {
        // dummy comment
        String text = "fooForOurClassfoos3";

        String delimiter = "o";
        Pattern pattern = Pattern.compile(delimiter);

        String[] stringsArray = pattern.split(text);

        for (String s:
                stringsArray) {
            System.out.println(s);
        }
    }
}
