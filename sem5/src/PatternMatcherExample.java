import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternMatcherExample {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile(".*foo.*");
        Matcher m = pattern.matcher("FoOabcFOOabcfoo");

        while (m.find()){
            System.out.println("Pattern has found from " + (m.start()) + " to " + (m.end()-1));
        }
    }
}
