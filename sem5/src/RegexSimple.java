import java.util.regex.Pattern;

public class RegexSimple {
    public static void main(String[] args){
        System.out.println(Pattern.matches("Name.*Surname", "cdwName random sequence Surname"));
        System.out.println(Pattern.matches("Name.*Surname", "Name random sequence Surname"));

//        "abc?" "? - 0, 1"
//        "abc*" "* >= 0"
//        "abc+" "+ >= 1"
//        "abc." "any"


//        "abc.*" "abc %any character zero or more times%"
//        "abc.?" "abc %any character zero or one times%"
//        "abc.+" "abc %any character one or more times%"


//        "abc{3}" "abccc"
//        "[0-9^7]$"
//        "[a-zA-Z]"
//        " mywords gdbn kdwd"
//        "infinite number of "1"'s and if "0" appears, than after that shall go "1" "
        //{0, 1}
        // 111111 - yes
        // 111110 - no
        // 1111101010101 - yes
    }
}
