import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ExecutorsExample {
    public static void main(String[] args) {
        ExecutorService executor = Executors.newSingleThreadExecutor();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.printf("Hello from %s\n", Thread.currentThread().getName());
                try {
                    TimeUnit.MILLISECONDS.sleep(100);
                    System.out.printf("Thread %s is finished \n", Thread.currentThread().getName());
                } catch (InterruptedException e) {
                    System.out.printf("Thread %s was interrupted \n", Thread.currentThread().getName());
                }
            }
        };

        executor.submit(runnable);
        executor.shutdownNow();
    }
}
