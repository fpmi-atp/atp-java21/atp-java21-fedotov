import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class AtomicIntegerExample {
    public static void main(String[] args) {
        raceCondition();

        noRaceCondition();
    }

    public static void raceCondition() {
        CustomInteger ci1 = new CustomInteger();

        Stream.iterate(0, n -> n + 1).limit(10_000).parallel().forEach(n->ci1.increment());

//        Long l1 = 100_000_000_000L; --> good formatting

        System.out.println(ci1.getVal());
    }

    public static void noRaceCondition() {
        CustomAtomic ca1 = new CustomAtomic();

        Stream.iterate(0, n -> n + 1).limit(10_000).parallel().forEach(n->ca1.increment());

        System.out.println(ca1.getVal());
    }

    static class CustomInteger {
        private int val = 0;

        public void increment() {
            val++;
        }

        public int getVal() {
            return val;
        }
    }

    static class CustomAtomic {
        private AtomicInteger ai = new AtomicInteger(0);

        public void increment() {
            ai.incrementAndGet();
        }

        public void decrement() {
            ai.decrementAndGet();
        }

        public AtomicInteger getVal() {
            return ai;
        }
    }

}
