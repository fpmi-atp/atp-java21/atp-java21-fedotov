import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockExample {


    static Object m = new Object();

    public static void main(String[] args) {
        // take lock here
        synchronized (m) {
            try {

            } catch (Exception e) {
            } finally {
                // release lock here
            }

        }
        // the lock will be released here
        boolean condition1 = true;

        Lock reentrantLock = new ReentrantLock();
        boolean condition2 = true;

        if (condition1) {
            // something
//            synchronized (m) { //take lock here
//        } // --> do not work

            reentrantLock.lock();

            if (condition2) {
                //something else
                reentrantLock.unlock();

                // release lock here
            }
        }
    }
}
