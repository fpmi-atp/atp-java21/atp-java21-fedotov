package reentrantlock.example;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SharedObject {
    int counter = 0;

    ReentrantLock lock = new ReentrantLock();

    void perfom() {
        lock.lock();
        System.out.println("Thread - " + Thread.currentThread().getName() + " acquired the lock");
        try {
            System.out.println("Thread - " + Thread.currentThread().getName() + " performing");
            counter++;
        } finally {
            lock.unlock();
            System.out.println("Thread - " + Thread.currentThread().getName() + " released the lock");
        }
    }

    void perfomTryLock() {
        System.out.println("Thread - " + Thread.currentThread().getName() + " is trying to access the lock");
        try {
            boolean isLockAcquired = lock.tryLock(20, TimeUnit.SECONDS);
            if (isLockAcquired) {
                try {
                    System.out.println("Thread - " + Thread.currentThread().getName() + " is performing");
                    lock.lock();
                    counter++;
                    Thread.sleep(1);
                } finally {
                    lock.unlock();
                    System.out.println("Thread - " + Thread.currentThread().getName() + " released the lock");
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public Lock getLock() {
        return lock;
    }

    public boolean isLocked(){
        return lock.isLocked();
    }

    public boolean waitingThreads(){
        return lock.hasQueuedThreads();
    }

    public int getCounter() {
        return counter;
    }

    public static void main(String[] args) {
        final int threadsCount = 2;

        final ExecutorService service = Executors.newFixedThreadPool(threadsCount);
        final SharedObject sharedObject = new SharedObject();

        service.execute(sharedObject::perfomTryLock);
        service.execute(sharedObject::perfomTryLock);

        service.shutdown();
    }
}

// timetout to wait the lock is 2 sec

// timeout 5 sec with 2 perfomTryLock(s):
//Thread - pool-1-thread-2 is trying to access the lock
//        Thread - pool-1-thread-1 is trying to access the lock
//        Thread - pool-1-thread-2 is trying to access the lock
//        Thread - pool-1-thread-1 is performing
//        Thread - pool-1-thread-1released the lock


// timeout 1 sec with 2 perfomTryLock(s):
//Thread - pool-1-thread-2 is trying to access the lock
//        Thread - pool-1-thread-1 is trying to access the lock
//        Thread - pool-1-thread-2 is performing
//        Thread - pool-1-thread-2 released the lock
