package reentrantlock.example;

import java.util.Stack;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockWithConditions {
    private Stack<String> stack = new Stack<>();
    private static final int CAPACITY = 5;

    private ReentrantLock lock = new ReentrantLock();
    private Condition stackEmptyCondition = lock.newCondition();
    private Condition stackFullCondition = lock.newCondition();

    private void pushToStack(String str) throws InterruptedException {
        try {
            lock.lock();
            if (stack.size() == CAPACITY) {
                System.out.println(Thread.currentThread().getName() + " waits on a full stack");
                stackFullCondition.await();
            }
            System.out.println("Pushing the item: " + str);
            stack.push(str);
            stackEmptyCondition.signalAll();
        } finally {
            lock.unlock();
        }
    }

    private String popFromStack() throws InterruptedException {
        try {
            lock.lock();
            if (stack.size() == 0) {
                System.out.println(Thread.currentThread().getName() + " waits on a empty stack");
                stackEmptyCondition.await();
            }
            return stack.pop();
        } finally {
            stackFullCondition.signalAll();
            lock.unlock();
        }
    }

    public static void main(String[] args) {
        final int threadCount = 2;
        final int iterationsCount = 10;
        ReentrantLockWithConditions lockWithConditions = new ReentrantLockWithConditions();
        final ExecutorService service = Executors.newFixedThreadPool(threadCount);

        service.execute(() -> {
            for (int i = 0; i < iterationsCount; i++) {
                try {
                    lockWithConditions.pushToStack("Item: " + i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        service.execute(() -> {
            for (int i = 0; i < iterationsCount; i++) {
                try {
                    System.out.println("Item is popped: " + lockWithConditions.popFromStack());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        service.shutdown();
    }
}

//run 1:
//pool-1-thread-2 waits on a empty stack
//        Pushing the item: Item: 0
//        Pushing the item: Item: 1
//        Pushing the item: Item: 2
//        Pushing the item: Item: 3
//        Pushing the item: Item: 4
//        Pushing the item: Item: 5
//        pool-1-thread-1 waits on a full stack
//        Item is popped: Item: 2
//        Item is popped: Item: 5
//        Pushing the item: Item: 6
//        Item is popped: Item: 6
//        Pushing the item: Item: 7
//        pool-1-thread-1 waits on a full stack
//        Item is popped: Item: 7
//        Pushing the item: Item: 8
//        pool-1-thread-1 waits on a full stack
//        Item is popped: Item: 8
//        Pushing the item: Item: 9
//        Item is popped: Item: 9
//        Item is popped: Item: 4
//        Item is popped: Item: 3
//        Item is popped: Item: 1
//        Item is popped: Item: 0


//run 2:
//        pool-1-thread-2 waits on a empty stack
//        Pushing the item: Item: 0
//        Pushing the item: Item: 1
//        Pushing the item: Item: 2
//        Pushing the item: Item: 3
//        Pushing the item: Item: 4
//        Pushing the item: Item: 5
//        Item is popped: Item: 0
//        pool-1-thread-1 waits on a full stack
//        Item is popped: Item: 5
//        Pushing the item: Item: 6
//        Item is popped: Item: 6
//        Pushing the item: Item: 7
//        pool-1-thread-1 waits on a full stack
//        Item is popped: Item: 7
//        Pushing the item: Item: 8
//        Pushing the item: Item: 9
//        Item is popped: Item: 8
//        Item is popped: Item: 9
//        Item is popped: Item: 4
//        Item is popped: Item: 3
//        Item is popped: Item: 2
//        Item is popped: Item: 1