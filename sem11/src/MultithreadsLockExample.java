import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MultithreadsLockExample {

    class BufferExample {
        final Lock lock = new ReentrantLock();

        final Condition notFull = lock.newCondition();
        final Condition notEmpty = lock.newCondition();
        final int bufferSize = 100;
        final Object[] items = new Object[bufferSize];
        int count, putpntr, getptr = 0;

        public void put(Object obj) throws InterruptedException {
            // TODO: if the buffer is full then wait until the buffer size becomes less than #bufferSize
            // TODO: only one thread can access this operation
            lock.lock();
            try {
                while (count == items.length) {
                    notFull.await();
                }

                items[putpntr] = obj;
                if (++putpntr == items.length) {
                    putpntr = 0;
                }
                count++;
                notEmpty.signal();
            } finally {
                lock.unlock();
            }
        }

        public Object get() throws InterruptedException {
            // TODO: read the last object from the queue that was not read
            // [a, b, c]. th1 reads a, th2 reads b, --> if there is a th3 it shall "c"
            // TODO: if the buffer is empty (items.size() == 0) than wait until items.size() > 0
            // TODO: only one thread can access this operation
            lock.lock();
            try {
                while (count == 0) {
                    notEmpty.await();
                }

                Object getObject = items[getptr];

                if (++getptr == items.length) {
                    getptr = 0;
                }
                count--;

                notFull.signal();

                return getObject;
            } finally {
                lock.unlock();
            }
        }
    }
}
