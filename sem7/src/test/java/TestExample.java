import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestExample {
    @Test
    public void testSimple(){
        assertEquals(4, ClassForTest.sumUp(1, 3));
        assertEquals(4, 3);
    }

}
