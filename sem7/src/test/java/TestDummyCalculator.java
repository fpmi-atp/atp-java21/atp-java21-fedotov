import org.junit.Test;

import java.util.Objects;

import static org.junit.Assert.assertEquals;

public class TestDummyCalculator {
    Calculator c = new Calculator();

    @Test
    public void addTest(){
        int result  = c.add(2, 2);

        assertEquals(result, 4);
    }

    @Test
    public void multiplyTest(){
        int result = c.multiply(3, 3);

//        assertEquals(result, 9);
        assertEquals(result, 5);

        Objects.equals(result, 5);
    }
}
