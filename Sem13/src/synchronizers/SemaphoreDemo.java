package synchronizers;

import java.util.concurrent.Semaphore;

public class SemaphoreDemo {
    static class Pool {
        private static final int MAX_AVAILABLE = 10;
        private final Semaphore semaphore = new Semaphore(MAX_AVAILABLE, true);

        public Object getItem() throws InterruptedException {
            semaphore.acquire();
            return getNextAvailableItem();
        }

        public void putItem(Object o) {
            if (markAsUnused(o)) {
                semaphore.release();
            }
        }

        Object[] itms = new Object[3];
        boolean[] used = new boolean[MAX_AVAILABLE];

        synchronized Object getNextAvailableItem() {
            for (int i = 0; i < MAX_AVAILABLE; i++) {
                if (!used[i]) {
                    used[i] = true;
                    return itms[i];
                }
            }
            return null;
        }

        synchronized boolean markAsUnused(Object item) {
            for (int i = 0; i < MAX_AVAILABLE; i++) {
                if (item == itms[i]) {
                    if (used[i]) {
                        used[i] = false;
                        return true;
                    } else return false;
                }
            }
            return false;
        }
    }
}
