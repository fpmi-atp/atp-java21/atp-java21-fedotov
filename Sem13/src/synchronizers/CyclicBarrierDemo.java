package synchronizers;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierDemo {
    static class Solver {
        static int threadsNum;
        static float[][] data;
        static CyclicBarrier barrier;

        static class Worker implements Runnable {
            int myRow;

            Worker(int row) {
                myRow = row;
            }

            @Override
            public void run() {
                float sum = 0;

                for (float elem : data[myRow]) {
                    sum += elem;
                }
                data[myRow][0] = sum;
                System.out.println(String.format("Thread %s is ready", Thread.currentThread().getName()));

                try {
                    barrier.await();
                } catch (InterruptedException | BrokenBarrierException e) {
                    e.printStackTrace();
                }

            }
        }

        public static void parallerMatrixSum(float[][] matrix) {
            data = matrix;
            threadsNum = matrix.length;
            barrier = new CyclicBarrier(threadsNum, () -> {
                float sum = 0;
                for (float[] floats : matrix) {
                    sum += floats[0];
                }
                System.out.println(String.format("The sum is %f", sum));
            });

            for (int i = 0; i < threadsNum; i++) {
                new Thread(new Worker(i)).start();
            }

        }
    }

    public static void main(String[] args) {
        Solver.parallerMatrixSum(new float[][]{{1F, 2F, 3F}, {4F, 5F, 6F}, {7F, 8F}});
    }

}
