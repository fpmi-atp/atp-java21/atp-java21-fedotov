import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentDataStructureDemo {
    final static int SIZE = 1_000_000;

    static long singleThreadHashMap() {
        long start = System.currentTimeMillis();

        Map<Integer, String> map = new HashMap<>(SIZE);
        for (int i = 0; i < SIZE; i++) {
            map.put(i, String.format("Value for integer %d", i));
        }

        return System.currentTimeMillis() - start;
    }

    static long multithreadingHashMap(int threadAmount) throws InterruptedException {
        assert threadAmount > 1;
        long start = System.currentTimeMillis();

        Map<Integer, String> map = new ConcurrentHashMap<>(SIZE);
        Thread[] threads = new Thread[threadAmount];

        for (int i = 0; i < threadAmount; i++) {
            int finalI = i;
            threads[i] = new Thread(() -> {
                int offset = SIZE / threadAmount;
                for (int k = 0; k < offset; k++) {
                    int key = finalI * offset + k;
                    map.put(key, String.format("Value for integer %d", key));
                }
            });
        }

        for (Thread thread : threads) {
            thread.start();
        }

        for (Thread thread : threads) {
            thread.join();
        }

        return System.currentTimeMillis() - start;
    }

    public static void main(String[] args) throws InterruptedException {
        System.out.println(String.format("Single thread implementation: %d", singleThreadHashMap()));

        System.out.println(String.format("Implementation with %d threads:: %d", 2, multithreadingHashMap(2)));
        System.out.println(String.format("Implementation with %d threads:: %d", 3, multithreadingHashMap(3)));
        System.out.println(String.format("Implementation with %d threads:: %d", 5, multithreadingHashMap(5)));
        System.out.println(String.format("Implementation with %d threads:: %d", 10, multithreadingHashMap(10)));
        System.out.println(String.format("Implementation with %d threads:: %d", 100, multithreadingHashMap(100)));
    }
}
