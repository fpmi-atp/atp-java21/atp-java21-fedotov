package markers;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ArrayListExample {


    public static void main(String[] args) {
        List<Double> doubleList = new LinkedList<>();
        List<Integer> integerList = new ArrayList<>(10);
        String str = "abd";
//        List<Character> myList = new ArrayList<Character>(List.of("some string".split("")));
        ArrayList<Double> notPtoperWay = new ArrayList<>();
        integerList.add(1);
        integerList.add(2);
        integerList.add(3);
//        ...
        integerList.add(8);



        integerList.forEach(System.out::println);
    }
}
