public class StringFormattingExample {

    private int i;
    private int j;
    private float k;

    StringFormattingExample(int i, int j, float k){
        this.i = i;
        this.j = j;
        this.k = k;
    }

    @Override
    public String toString() {
        return String.format("StringFormattingExample{" +
                "i=%d" +
                ", j=%d" +
                ", k=%2.3f" +
                '}', i, j, k);
    }
}
