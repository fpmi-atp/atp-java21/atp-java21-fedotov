import java.util.Random;
import java.util.stream.Stream;

public class StremasGenerateExample {
    public static void main(String[] args) {
        Stream<Integer> randomIntegers = Stream.generate(() -> (new Random().nextInt(100)));

        randomIntegers.limit(2).forEach(System.out::println);

        // output1: 2, 32
        // output1: 16, 64
        // output1: 2, 8
    }
}
