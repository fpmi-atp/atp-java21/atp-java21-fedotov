import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StreamsOperations {

    public static void main(String[] args) {
        List<String> listNames = new ArrayList<>();
        listNames.add("John");
        listNames.add("Frederico");
        listNames.add("Folu");
        listNames.add("Gabriel");
        listNames.add("Rohan");
        listNames.add("Ivan");

        List<String> outputFromStream = listNames.stream().filter(s->s.startsWith("F"))
                .map(String::toUpperCase)
                .sorted()
                .collect(Collectors.toList());

        Map<Integer, Integer> map = new HashMap<>(); // --> no stream

        System.out.println(outputFromStream);


        boolean anyMatchedResult = listNames.stream()
                .anyMatch(s->s.startsWith("F"));
        System.out.printf("Any starts with F: %b%n", anyMatchedResult);

        boolean allMatchedResult = listNames.stream()
                .allMatch(s->s.startsWith("A"));
        System.out.printf("All starts with A: %b%n", allMatchedResult);

        boolean noneMatchedResult = listNames.stream()
                .noneMatch(s->s.startsWith("S"));
        System.out.printf("None starts with S: %b%n", noneMatchedResult);

        String firstMatchedName = listNames.stream()
                .filter(s->s.startsWith("F"))
                .findFirst()
                .get();
    }
}
