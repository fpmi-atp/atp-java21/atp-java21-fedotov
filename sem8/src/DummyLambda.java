import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DummyLambda {
    public static void main(String[] args) {
        List<Integer> integerList = new ArrayList<>();
        integerList.add(0);
        integerList.add(1);
        integerList.add(2);

//        integerList.forEach(i -> System.out.println(i));

//        for (Integer i :
//                integerList) {
//            System.out.println(i);
//        }

        long cnt = integerList.stream()
                .filter(i -> i > 0)
                .count();

        System.out.println(String.format("Number of positive integers: %d", cnt));

        List<String> stringsLust = Arrays.asList("abc", "cde", "");

        long emptyStrings = stringsLust.stream()
                .filter(x->x.isEmpty())
                .count();

        System.out.println(String.format("number of empty strings: %d", emptyStrings));


    }
}
