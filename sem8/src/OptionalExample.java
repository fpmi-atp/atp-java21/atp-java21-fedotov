import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class OptionalExample {

    public static void main(String[] args) {
        List<String> listNames = new ArrayList<>();
        listNames.add("John");
        listNames.add("Frederico");
        listNames.add("Folu");
        listNames.add("Gabriel");
        listNames.add("Rohan");
        listNames.add("Ivan");

        Optional<String> nameOptionalDoesNotExist = listNames.stream().filter(s->s.startsWith("A"))
                .findAny();

//        System.out.println(nameOptionalDoesNotExist.get()); --> java.util.NoSuchElementException

        nameOptionalDoesNotExist.ifPresent(System.out::println);

        Optional<String> nameOptionalDoesExist = listNames.stream().filter(s->s.startsWith("F"))
                .findAny();

        if(nameOptionalDoesExist.isPresent()){
            System.out.println(nameOptionalDoesExist.get());
        }
    }
}
