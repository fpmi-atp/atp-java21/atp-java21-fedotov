import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

public class FluxExample {
    public static void main(String[] args) {
        // many elements
        Flux<Integer> justFlux = Flux.just(1, 2, 3, 4, 5);

        List<Integer> elements = new ArrayList<>();
        elements.add(6);
        elements.add(7);

        // one element
        Mono<Integer> justMono = Mono.just(1);

        justFlux.log().subscribe(elements::add);

        System.out.println("elements in my flux: ");
        elements.forEach(x -> System.out.println(x));
    }
}
