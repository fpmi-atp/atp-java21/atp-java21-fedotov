import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TimeExample {
    public static void main(String[] args) {
        LocalDate l = LocalDate.now();
        System.out.println("Current rime: " + l);
        // format1: DD.MM.YYYY
        // format2: MM.DD.YY
        // format3: ...
        System.out.println("Current rime in another format: " + l.format(DateTimeFormatter.ofPattern("dd::MMMM::uu")));


        System.out.println("Current rime in another format: " + l.format(DateTimeFormatter.BASIC_ISO_DATE));


        LocalDateTime lDateTime = LocalDateTime.now();

        System.out.println("Current rime in another format: " + lDateTime.format(DateTimeFormatter.ofPattern("d::MMM::uu HH::mm::ss")));

        // brackets validator task:
        // {{}} - valid
        // {{}}} - non-valid
        // More difficult: ([{}]) - valid
        // {([}]) - non-valid

        // task: write the method
        // input: string, representation of brackets
        // output: boolean
    }
}
