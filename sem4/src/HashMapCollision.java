import java.util.HashMap;

public class HashMapCollision extends HashMap {
    @Override
    public int hashCode() {
        return 1;
    }
}

// (k, v) --> [hashcode(k) % n] == const --> put in the same bucket