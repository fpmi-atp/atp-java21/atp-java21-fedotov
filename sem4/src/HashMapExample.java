import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class HashMapExample {

    public void example() {
        Map<Integer, String> integerStringMap = new HashMapCollision();

        integerStringMap.put(1, "amount");
        integerStringMap.put(3, "amount2");
        integerStringMap.put(5, "amount2");
//        integerStringMap.put(2, "test");

//        integerStringMap.forEach((k, v)-> System.out.printf("key: %d, value: %s  %n", k, v));

        Map<Integer, String> hashMapSecondExample = new HashMap<>();
        hashMapSecondExample.put(12, "John Gold");
        hashMapSecondExample.put(15, "Bob Schwarz");
        hashMapSecondExample.put(17, "Fernando Raskoni");
        System.out.println(hashMapSecondExample.get(12));

        Set valuesSet = hashMapSecondExample.entrySet();
        Set keysSet = hashMapSecondExample.keySet();

        hashMapSecondExample.remove(17);

        System.out.println(Objects.isNull(hashMapSecondExample.get(17)));


    }
}
