import java.time.LocalDate;
import java.time.Month;

public class DateExample {
    public static void main(String[] args) {
        LocalDate today = LocalDate.now();
        System.out.println("today is: " + today);

        LocalDate randomDate = LocalDate.of(2021, Month.AUGUST, 1);
        System.out.println(randomDate);

        LocalDate whatTheBeatifulDate = LocalDate.ofYearDay(2021, 256);
        System.out.println(whatTheBeatifulDate);

        System.out.println(randomDate.isAfter(whatTheBeatifulDate));
        System.out.println(randomDate.isBefore(whatTheBeatifulDate));

        LocalDate anotherRandomDate = randomDate.minusMonths(1);

        System.out.println(randomDate.minusMonths(1));
        System.out.println(randomDate.minusMonths(1));
        System.out.println(anotherRandomDate.minusMonths(1));
//        System.out.println(randomDate.minusMonths(5));
//        System.out.println(randomDate.minusWeeks(5));
    }
}
