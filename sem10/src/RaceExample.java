import java.util.stream.Stream;

public class RaceExample {
    public static void main(String[] args) throws InterruptedException {
        Reordering.main();
    }

    public static void raceCondition() {
        // There is no race
        Increment inc = new Increment();
        Stream.iterate(0, n -> n + 1).limit(100_000).forEach(n -> inc.increment());
        System.out.println(inc.getValue());

        Increment inc1 = new Increment();
        Stream.iterate(0, n -> n + 1).limit(100_000).parallel().forEach(n -> inc1.increment());
        System.out.println(inc1.getValue());
    }

    static class Increment {
        private int value = 0;

        void increment() {
            value++;
        }

        public int getValue() {
            return value;
        }
    }

    static class Reordering {
        static int x = 0;
        static int y = 0;
        static int a = 0;
        static int b = 0;

        public static void main() throws InterruptedException {
            Thread t1 = new Thread(() -> {
                a = 1;
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                x = b;
            });

            Thread t2 = new Thread(() -> {
                b = 1;
                y = a;
            });

            t1.start();
            t2.start();

            t1.join();
            t2.join();

            System.out.println(String.format("Here are my vars: x = %d, y = %d", x, y));
        }
    }
}
