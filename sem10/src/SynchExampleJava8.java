import java.util.stream.Stream;

public class SynchExampleJava8 {
    public static void main(String[] args) {
        Increment inct = new Increment();
        long start = System.currentTimeMillis();
        Stream.iterate(0, n -> n + 1).limit(100_000_000).parallel().forEach(n -> inct.increment());
        long executionTime = System.currentTimeMillis() - start;

        System.out.println(inct.getVal());
        System.out.println(String.format("Execution time: %d", executionTime));

    }

    /*
    synch: 14044
    non-synch: 7178
     */

    static class Increment {
        private int val = 0;

        synchronized void increment() {
            val++;
        }

        public int getVal() {
            return val;
        }
    }
}
