import java.util.Random;

public class ExampleMultithreadingPerformance {
    private static int[] initArray() {
        int size = 100_000_000;

        int array[] = new int[size];

        Random random = new Random();

        for (int i = 0; i < size; i++) {
            array[i] = random.nextInt(100);
        }
        return array;
    }

    public static void singleThreadSolution(int[] array) {
        long result = 0;

        long start = System.currentTimeMillis();

        for (int val :
                array) {
            result += val;
        }

        long exectime = System.currentTimeMillis() - start;

        System.out.printf("Single thread solution. Sum = %d, execution time = %d%n", result, exectime);
    }

    public static void multiThreadSolution(int threadsNum, int[] array) throws InterruptedException {
        assert threadsNum > 1;

        long result = 0;
        long start = System.currentTimeMillis();

        int intervalLength = array.length / threadsNum;
        Worker[] workers = new Worker[threadsNum];

        for(int i = 0; i < threadsNum; i++){
            workers[i] = new Worker(array, i*intervalLength, (i+1)*intervalLength);
        }

        for (Worker w:
                workers) {
            w.start();
        }


        for (Worker w:
                workers) {
            w.join();
            result+= w.getPartitialSum();
        }

        long exectime = System.currentTimeMillis() - start;

        System.out.printf("Multi thread solution. Sum = %d, execution time = %d%n", result, exectime);
    }

    public static void main(String[] args) {
        int[] array = initArray();
        singleThreadSolution(array);

        try {
            multiThreadSolution(64, array);
            /*
            1 thread: 157
            2 threads:110
            3 threads:85
            4 threads:72
            5 threads:66
            ...
            64 threads: 136 - increase because of the time spent on run/stop threads
             */
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    static class Worker extends Thread{
        private int[] array;
        int partitialSum = 0;
        int from ;
        int to;

        Worker(int[] array, int from, int to){
            this.array = array;
            this.from = from;
            this.to = to;
        }

        public void run() {
            for(int  i = from; i < to; i++){
                partitialSum += array[i];
            }
        }

        public int getPartitialSum() {
            return partitialSum;
        }
    }
}
