import java.util.ArrayList;
import java.util.List;

public class GenericsExample {
    public static void main(String[] args) {
        MyFirstGeneric<IntegerExtension> myFirstGeneric = new MyFirstGeneric();
        myFirstGeneric.classGenericType(new IntegerExtension());
//        myFirstGeneric.classGenericType(new Double(1d)); --> will not compile, as IntegerExtension is already specified


        List<Integer> list = new ArrayList<>();
        list.add(1);
        myFirstGeneric.genericMethod(list);


        List<Double> listDouble = new ArrayList<>();
        listDouble.add(1d);

        myFirstGeneric.genericMethod(listDouble);
//        myFirstGeneric.genericMethod(new IntegerExtension());


//        list.add(new Integer(1));
//
//        Integer i = list.iterator().next();

//        System.out.println(myFirstGeneric.secondGenericMethod(new IntegerExtension()).getClass());

        List<Integer> integerList = new ArrayList<>();
        integerList.add(0);
        integerList.add(2);
        integerList.add(5);
        integerList.add(7);
        integerList.forEach(System.out::println);

        Swapper.swap(integerList, 1, 3);

        integerList.forEach(System.out::println);
    }
}

// input: list, 2 indexes; {1, 2, 3}; 0, 2
// output: list with swapped elements {3, 2, 1}
//