import java.util.List;

public class Swapper {
    public static <T> void swap(List<T> list, int first, int second){
        T firstElement = list.get(first);

        list.set(first, list.get(second));
        list.set(second, firstElement);
    }
}
