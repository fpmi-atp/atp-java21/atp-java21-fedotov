import sem6.RentalCar;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class ReflectionExample {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchFieldException {
        Class rental = RentalCar.class;

        System.out.println("Class Name is: " + rental.getName());
        System.out.println("Simple class Name is: " + rental.getSimpleName());

        System.out.println("Constructor is: " + rental.getConstructors()[0]);
        Constructor constructor = rental.getConstructor(Integer.TYPE);
        System.out.println("Constructor is: " + constructor);

        RentalCar rentalInstance = (RentalCar) constructor.newInstance(355);
//        RentalCar car = new RentalCar(355); --> the same

        Method[] allMethods = rental.getMethods();
        System.out.println("Methods are: " + Arrays.toString(allMethods));

        Method[] literallyAllMethods = rental.getDeclaredMethods();
        System.out.println("Methods are: " + Arrays.toString(literallyAllMethods));

        Method notSecretMethod = rental.getMethod("computeRentalPrice", new Class[]{Integer.TYPE});
        System.out.println(notSecretMethod.getName());

        notSecretMethod.invoke(rentalInstance, 4);

        rentalInstance.computeRentalPrice(4);


        Field privateField = RentalCar.class.getDeclaredField("rate");
        System.out.println("here is our private field: " + privateField.getName());
        privateField.setAccessible(true);
        System.out.println("here is our private field: " + privateField.get(rentalInstance));


        Method verySecretMethod = rental.getMethod("verySecretMethod", new Class[]{});
        verySecretMethod.setAccessible(true);
        verySecretMethod.invoke(rentalInstance);

    }
}
