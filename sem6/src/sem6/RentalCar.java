package sem6;

public class RentalCar {
    protected String protectedExample;
    private int rate;
    private String type;
    public int price;

    private final int BIG_LENGTH = 500;
    private final int SMALL_LENGTH = 300;

    public RentalCar(int length){
        if(length < SMALL_LENGTH){
            type ="small";
            rate = 7;
        }
        else if (length >= SMALL_LENGTH && length <= BIG_LENGTH){
            type ="middle";
            rate = 11;
        }
        else if (length > BIG_LENGTH){
            type ="big";
            rate = 15;
        }
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void computeRentalPrice(int timeRentalInterval){
        price = rate * timeRentalInterval;
        System.out.println(String.format("The final cost is %d", price));
    }

    private void verySecretMethod(int secretArgument){
        System.out.println("so secret " + secretArgument);
    }
}
