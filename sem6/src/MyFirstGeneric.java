import java.util.List;

public class MyFirstGeneric<N extends Number> {

    public void genericMethod(List<? extends Number> numbers) {

        System.out.println(numbers.get(0).getClass());
    }

    public void classGenericType(N arg){
        System.out.println(arg.getClass());
    }
}
