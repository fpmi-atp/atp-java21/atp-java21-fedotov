import java.util.concurrent.atomic.AtomicInteger;

public class ThreadEvenNumbers implements Runnable {
    AtomicInteger i;
    int limit;

    ThreadEvenNumbers(AtomicInteger i, int limit) {
        this.i = i;
        this.limit = limit;
    }

    @Override
    public void run() {
        for (int k = 0; k < limit; k++) {
            if (k % 2 == 0) {
                System.out.println(i.incrementAndGet());
            }
        }
    }
}
