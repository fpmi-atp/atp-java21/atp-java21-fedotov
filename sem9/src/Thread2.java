public class Thread2 extends Thread {
    Computation c;
    Object o;

    Thread2(Computation c, Object o){
        this.c = c;
        this.o = o;
    }

    public void run(){
        c.computeValue(100, o);
    }
}
