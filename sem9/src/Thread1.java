public class Thread1 extends Thread{
    Computation c;
    Object o;

    Thread1(Computation c, Object o){
        this.c = c;
        this.o = o;
    }

    public void run(){
        c.computeValue(5, o);
    }
}
