import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    public static void main(String[] args) {
        AtomicInteger ai = new AtomicInteger();
        int limit  = 10;

        ThreadEvenNumbers threadEvenNumbers = new ThreadEvenNumbers(ai, limit);
        ThreadOddNumbers threadOddNumbers = new ThreadOddNumbers(ai, limit);

        threadEvenNumbers.run();
        threadOddNumbers.run();
    }
}
