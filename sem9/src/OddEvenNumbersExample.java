public class OddEvenNumbersExample {
    int counter = 1;
    static int N;

    public void printOddNumbers() {
        synchronized (this) {
            while (counter < N) {
                while (counter % 2 == 0) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                System.out.println(counter);
                counter++;
            }
            notify();
        }
    }

    public void printEvenNumbers() {
        synchronized (this) {
            if (counter < N) {
                while (counter % 2 == 0) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(counter);
                    counter++;
                }
                notify();
            }
        }
    }

    public static void main(String[] args) {
        OddEvenNumbersExample example = new OddEvenNumbersExample();
        OddEvenNumbersExample.N = 10;

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                example.printEvenNumbers();
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                example.printOddNumbers();
            }
        });

        t1.start();
        t2.start();
    }
}
