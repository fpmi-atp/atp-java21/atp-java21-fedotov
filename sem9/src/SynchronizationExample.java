public class SynchronizationExample {
    public static void main(String[] args) {
        Computation c = new Computation();
        Object monitor1 = new Object();
        Object monitor2 = new Object();

        Thread1 t1 = new Thread1(c, monitor1);

        Thread2 t2 = new Thread2(c, monitor2);

        t1.start();
        t2.start();
    }
}

/*
1. create 2 threads
2. pass parameter "n" to each thread
3. thread 1 prints odd numbers between 0 and "n"
4. thread 2 prints even numbers between 0 and "n"
5. threads must be synchronized

 5 - as a parameter
example without synch: 13524
example with synch: 12345
 */
