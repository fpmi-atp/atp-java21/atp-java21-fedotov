public class Computation {
    void computeValue(int n, Object o) {
        synchronized (o) {
            for (int i = 0; i < 5; i++) {
                System.out.println(i * n);
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
